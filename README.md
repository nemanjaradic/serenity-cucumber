# Serenity Cucumber project 

## About
This is a test project which goal is to present REST Api testing using Java, Cucumber and Serenity.

Documentation : https://gorest.co.in/

Resource under test : 
https://gorest.co.in/public-api/users
        
        POST /public-api/users                      Create a new user
        GET /public-api/users/{userId}              Get user details
        PUT /public-api/users/{userId}              Update user details
        DELETE /public-api/users/{userId}           Delete user
Request methods PUT, POST, DELETE needs access token, which needs to be passed in "Authorization" header as Bearer token.
## Prerequisites
-Java jdk 11.0.11  
-apache-maven-3.8.1 

## Project structure
Step definitions : starter.stepdefinitions.UserStepDef

API Steps : starter.api.UserApi

Feature files : resources.features


## Running the tests
In order to execute all tests you need to run maven command : 
mvn clean verify -Denvironment=dev serenity:aggregate

In order to execute tagged tests you need to run maven command: 
mvn clean verify -Denvironment=dev -Dcucumber.filter.tags=@<!-- -->SMOKE serenity:aggregate

Test results/Serenity reports : ./target/site/serenity/index.html