package model;

public class User {

	private String name;
	private String email;
	private String gender;
	private UserStatus status;

	private User(Builder builder) {
		this.name = builder.name;
		this.email = builder.email;
		this.gender = builder.gender;
		this.status = builder.status;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}

	public static class Builder {

		private String name;
		private String email;
		private String gender;
		private UserStatus status;

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder email(String email) {
			this.email = email;
			return this;
		}

		public Builder gender(String gender) {
			this.gender = gender;
			return this;
		}

		public Builder status(UserStatus status) {
			this.status = status;
			return this;
		}

		public User build() {
			return new User(this);
		}

	}

}
