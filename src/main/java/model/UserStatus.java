package model;

public enum UserStatus {
	Active, Inactive, Closed
}
