package starter.utils;

import org.apache.commons.lang3.RandomStringUtils;

public class TestUtils {

	public static String getRandomValue() {
		return RandomStringUtils.randomAlphabetic(5);

	}
}
