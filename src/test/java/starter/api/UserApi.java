package starter.api;

import net.thucydides.core.annotations.Step;
import net.serenitybdd.rest.SerenityRest;

import io.restassured.response.ValidatableResponse;
import model.User;
import starter.base.TestBase;

public class UserApi {

	@Step("Send post request to create new user")
	public ValidatableResponse postRequestCreateUser(User user, boolean authorization) {
		return SerenityRest.rest()
			.given()
			.spec(TestBase.setRequestSpec(authorization))
			.when()
			.body(user)
			.post("/users/")
			.then()
			.log()
			.all();
	}

	@Step("Send put request to update a user")
	public ValidatableResponse putRequestUpdateUser(User user, int userId) {
		return SerenityRest.rest()
			.given()
			.spec(TestBase.setRequestSpec(true))
			.when()
			.body(user)
			.put("/users/" + userId)
			.then().log().all();
	}

	@Step("Send get request with user id to retrieve user information")
	public ValidatableResponse getUserById(int userId) {
		return SerenityRest
			.rest()
			.given()
			.spec(TestBase.setRequestSpec(false))
			.when()
			.get("/users/" + userId)
			.then();
	}

	@Step("Send delete request with user id to delete user information")
	public ValidatableResponse deleteUserById(int userId) {
		return SerenityRest
			.rest()
			.given()
			.spec(TestBase.setRequestSpec(true))
			.when()
			.delete("/users/" + userId)
			.then();
	}
}
