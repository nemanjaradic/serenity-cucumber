package starter.stepdefinitions;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;
import java.util.Map;

import net.thucydides.core.annotations.Steps;

import org.hamcrest.Matchers;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.ValidatableResponse;
import model.UserStatus;
import model.User;
import starter.api.UserApi;
import starter.utils.TestUtils;

public class UserStepDef {

    Integer userId;
    ValidatableResponse response;
    User userObject;
    @Steps
    UserApi userApi;

    @When("^I create new user with following values:$")
    public void create_new_user(List<Map<String, String>> data
    ) {
        userObject = buildUserObject(data);
        response = userApi.postRequestCreateUser(userObject, true);
    }

    @When("^I want to create new user with listed values and without authorization:$")
    public void create_new_user_no_auth(List<Map<String, String>> data) {
        userObject = buildUserObject(data);
        response = userApi.postRequestCreateUser(userObject, false);
    }

    @When("^I update user with name (.*) and gender (.*)$")
    public void update_user_information(String name, String gender) {
        userObject.setName(name);
        userObject.setGender(gender);
        response = userApi.putRequestUpdateUser(userObject, userId);
    }

    @When("^I send create user request with same email as in previous step")
    public void send_post_user_request() {
        response = userApi.postRequestCreateUser(userObject, true);
    }

    @When("^I search for user by non existing id$")
    public void send_get_user_by_id_max_int() {
        response = userApi.getUserById(Integer.MAX_VALUE);
    }

    @When("^I search for user by id$")
    public void send_get_user_by_id_request() {
        response = userApi.getUserById(userId);
    }

    @When("^I delete newly created user by id$")
    public void send_delete_user_request() {
        response = userApi.deleteUserById(userId);
    }

    @Then("^Status code is (.*)$")
    public void retrieve_status_code(Integer statusCode) {
        response.assertThat()
                .body("code", Matchers.equalTo(statusCode));
        if (statusCode == 201) {
            userId = response.extract()
                    .path("data.id");
        }
    }

    @Then("^Response contains details$")
    public void response_contains_details(List<Map<String, String>> data) {
        data.get(0).forEach((key, value) -> assertThat(response.extract().asString()
                .replace("\"", ""), containsString(key.concat(":").concat(value))));
    }

    private User buildUserObject(final List<Map<String, String>> data) {
        return new User.Builder().name(data.get(0).get("name"))
                .gender(data.get(0).get("gender"))
                .status(UserStatus.valueOf(data.get(0).get("status")))
                .email(TestUtils.getRandomValue()
                        + (data.get(0).get("email_suffix")))
                .build();
    }

    public void feature4() {
        System.out.println("feature4");
    }

    public void feature5() {
        System.out.println("feature5");
    }
}
