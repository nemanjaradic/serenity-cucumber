package starter.base;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class TestBase {

	private static final String AUTH_TOKEN = "Bearer b7350b2109198170d424699dde91d26fc5f628ff3008fcdf6aaddaac0b36b7df";
	private static EnvironmentVariables environmentVariables = SystemEnvironmentVariables.createEnvironmentVariables();
	private static final String BASE_URL = EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("base.url");

	public static RequestSpecification setRequestSpec(boolean auth) {
		final RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
		final RequestSpecification requestSpecification = requestSpecBuilder.build();

		requestSpecBuilder.setBaseUri(BASE_URL);
		if (auth) {
			requestSpecBuilder.addHeader("Authorization", AUTH_TOKEN);
		}
		requestSpecBuilder.setContentType(ContentType.JSON);

		return requestSpecification;
	}
}
