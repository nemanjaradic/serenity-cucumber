Feature:Create user test scenarios


	Scenario Outline: Create new user and verify response body
		When I create new user with following values:
			| name       | gender       | status       | email_suffix  |
			| <userName> | <userGender> | <userStatus> | <emailSuffix> |
		Then Status code is 201
		And Response contains details
			| name       | gender       | status       |
			| <userName> | <userGender> | <userStatus> |
		Examples:
			| userName | userGender | userStatus | emailSuffix          |
			| Mark     | Male       | Active     | _jhondoe@testnet.com |
			| Jhon     | Male       | Inactive   | _jhondoe@testnet.com |
			| Rachel   | Female     | Active     | _jhondoe@testnet.com |
			| Anna     | Female     | Inactive   | _jhondoe@testnet.com |

	Scenario: Email is unique for every user record
		When  I create new user with following values:
			| name   | gender | status | email_suffix         |
			| Rachel | Female | Active | _jhondoe@testnet.com |
		Then Status code is 201
		And Response contains details
			| name   | gender | status |
			| Rachel | Female | Active |
		When I send create user request with same email as in previous step
		Then Status code is 422
		And Response contains details
			| field | message                |
			| email | has already been taken |


	Scenario: Create new user without first name
		When  I create new user with following values:
			| name | gender | status | email_suffix         |
			|      | Male   | Active | _jhondoe@testnet.com |
		Then Status code is 422
		And Response contains details
			| field | message        |
			| name  | can't be blank |


	Scenario: Create new user with invalid gender
		When  I create new user with following values:
			| name | gender | status | email_suffix         |
			| Jhon | Test   | Active | _jhondoe@testnet.com |
		Then Status code is 422
		And Response contains details
			| message               |
			| can be Male or Female |

	Scenario: Create new user without gender
		When  I create new user with following values:
			| name | gender | status | email_suffix         |
			| Jhon |        | Active | _jhondoe@testnet.com |
		Then Status code is 422
		And Response contains details
			| field  | message        |
			| gender | can't be blank |

	Scenario: Create new user with invalid email
		When I create new user with following values:
			| name | gender | status | email_suffix |
			| Jhon | Male   | Active | s            |
		Then Status code is 422
		And Response contains details
			| field | message    |
			| email | is invalid |

	Scenario: Create new user with invalid status
		When  I create new user with following values:
			| name   | gender | status | email_suffix         |
			| Rachel | Female | Closed | _jhondoe@testnet.com |
		Then Status code is 422
		And Response contains details
			| field  | message                   |
			| status | can be Active or Inactive |


	@SMOKE
	Scenario: Create new user without authorization
		When  I want to create new user with listed values and without authorization:
			| name | gender | status | email_suffix         |
			| Jhon | Male   | Active | _jhondoe@testnet.com |
		Then Status code is 401
		And Response contains details
			| message               |
			| Authentication failed |



