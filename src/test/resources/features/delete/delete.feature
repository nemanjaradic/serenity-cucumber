Feature: Delete user test scenarios

	Background:
		When  I create new user with following values:
			| name | gender | status | email_suffix         |
			| Mark | Male   | Active | _jhondoe@testnet.com |
		Then Status code is 201

	@SMOKE
	Scenario: Delete user newly created user
		When I delete newly created user by id
		Then Status code is 204

	Scenario: Delete same user two times
		When I delete newly created user by id
		Then Status code is 204
		And I delete newly created user by id
		Then Response contains details
			| message            |
			| Resource not found |
