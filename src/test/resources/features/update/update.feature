Feature: Update user test scenarios

	Background:
		When I create new user with following values:
			| name | gender | status | email_suffix         |
			| Jhon | Male   | Active | _jhondoe@testnet.com |
		Then Status code is 201

	@SMOKE
	Scenario: Create user, then change name and gender of the user
		When I update user with name Britney and gender Female
		Then Status code is 200
		And Response contains details
			| name    | gender | status |
			| Britney | Female | Active |


	Scenario: Create user, then update user name with empty value
		When I update user with name  and gender Female
		Then Status code is 422
		And Response contains details
			| field | message        |
			| name  | can't be blank |

	Scenario: Create user, then update user name with incorrect gender
		When I update user with name Alex and gender n/a
		Then Status code is 422
		And Response contains details
			| field  | message               |
			| gender | can be Male or Female |