Feature: Get user test scenarios

	Scenario: Create user, then search for user by user ID
		When I create new user with following values:
			| name | gender | status | email_suffix         |
			| Jhon | Male   | Active | _jhondoe@testnet.com |
		Then Status code is 201
		When I search for user by id
		Then Status code is 200
		And Response contains details
			| name | gender | status |
			| Jhon | Male   | Active |

	Scenario: Non existing id returns 404
		When I search for user by non existing id
		Then Status code is 404
		And Response contains details
			| message            |
			| Resource not found |